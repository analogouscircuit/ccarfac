import array
import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack as fft
from ctopy import ctopy_read

def main(BM_out, IHC_out, sig, fs):
    num_sections, num_points = BM_out.shape
    print(num_sections, num_points)
    myFFT = fft.fft(np.zeros((num_sections, num_points)))
    for s in range(num_sections):
        myFFT[s] = (fft.fft(BM_out[s,:])/fft.fft(sig))
    fig = plt.figure(1, figsize=(12,4)) # Bode plot of BM displacement 
    ax1 = fig.add_subplot(1,2,1)
    freq = np.linspace(0,fs/2.0,num_points//2)
    ax1.semilogx(freq,20*np.log10(abs(myFFT.T[:int((num_points)/2),:])+1e-10))  # note, 1e-10 offset to avoid division by zero in log10
    ax1.set_title('BM gain (in dB)')
    ax1.set_ylim([-100, 60])
    ax1.set_xlabel('f (in Hz)')
    ax2 = fig.add_subplot(1,2,2,sharex=ax1)
    ax2.semilogx(freq,np.unwrap(np.angle(myFFT.T[0:int((num_points)/2),:]),discont=5,axis=0))
    ax2.set_title('BM phase (in rad)')
    ax2.set_ylim([-5, 2])
    ax2.set_xlabel('f (in Hz)')
    fig.tight_layout()

    ## plot impulse response
    IR = np.real(fft.ifft(myFFT))
    IR[:,0] = np.zeros(num_sections)

    fig = plt.figure(2, figsize=(12,4))
    L=600
    plt.plot(np.arange(L)*1000/fs, IR[:,0:L].T)
    plt.xlabel('t (in ms)')
    plt.title("BM Impulse Response")
    plt.tight_layout()

    ## plot IHC response
    t1 = np.arange(num_points)/fs
    fig = plt.figure(3, figsize=(12,4))
    plt.plot(t1*1000, sig, 'r')
    plt.plot(t1*1000, IHC_out.T*2.0)
    plt.xlabel('t (in ms)')
    plt.title('IHC response')
    plt.tight_layout()

    ## staggered plot of IHC output
    fig = plt.figure(4)
    t_max = num_points/fs
    num_points = int(t_max*fs)
    t = np.arange(num_points)/fs
    p = 0.01
    for k in range(num_sections):
        plt.fill_between(t, 0, IHC_out[k,:num_points]+p*(num_sections-k), facecolor='w',
                edgecolor='k', linewidth=0.6)


    plt.show()

## Main script
if __name__=="__main__":
    # import data
    imported_data_dict = ctopy_read("carfac_test_data")
    BM_out = imported_data_dict['bm_out']
    IHC_out = imported_data_dict['ihc_out']
    fs = imported_data_dict['fs']
    sig = imported_data_dict['sig']
    f_vals = imported_data_dict['f_vals']
    print("Channel CFs: ", f_vals)

    # run script
    main(BM_out, IHC_out, sig, fs)
