import numpy as np
import scipy.signal as dsp
import scipy.fftpack as fft
import matplotlib.pyplot as plt
import scipy.io.wavfile as wav

def damped_env(T, tau, num_samples, fs):
    t_period = np.arange(0,T,1/fs) 
    period = np.exp(-t_period/tau)
    n_period = len(period)
    env = [period[k%n_period] for k in range(num_samples)]
    return env

if __name__=="__main__":
    dur = 1.0
    fs = 32000
    f0 = 100.0
    dt = 1/fs
    t = np.arange(0,dur,dt)
    sig = np.sin(2*np.pi*f0*t)
    period = damped_env(0.10, 0.050, len(sig), fs)
    sig = sig*period
    plt.plot(sig)
    plt.show()
    wav.write("damped_sinusoid.wav", fs, sig)
