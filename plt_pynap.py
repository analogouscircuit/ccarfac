import numpy as np
import scipy.signal as dsp
import matplotlib.pyplot as plt

def carfac_agc(stimulus, fs, num_sect, x_lo=0.1, x_hi=0.9, show_progress=False):
    '''
    Returns the neural activity pattern (NAP) for a monaural input, i.e. the
    inner-hair cell (IHC) response for the chosen number of chochlear sections
    (channels)

    This is essentially just an encapsulation of Andre van Schaik's Jupyter
    notebook implementation of the carfac-agc model.
    '''
    dur = len(stimulus)/fs        # stimulus length
    npoints = int(fs*dur)

    # BM parameters
    x = np.linspace(x_hi,x_lo,num_sect) # position along the cochlea 1 = base, 0 = apex
    f = 165.4*(10**(2.1*x)-1)     # Greenwood for humans
    a0 = np.cos(2*np.pi*f/fs)           # a0 and c0 control the poles and zeros
    c0 = np.sin(2*np.pi*f/fs)

    damping = 0.2                 # damping factor
    r = 1 - damping*2*np.pi*f/fs     # pole & zero radius actual
    r1 = 1 - damping*2*np.pi*f/fs    # pole & zero radius minimum (set point)
    h = c0                        # p302 h=c0 puts the zeros 1/2 octave above poles
    g = (1-2*a0*r+r*r)/(1-(2*a0-h*c0)*r+r*r)  # p303 this gives 0dB DC gain for BM

    f_hpf = 20                    # p328 20Hz corner for the BM HPF
    q = 1/(1+(2*np.pi*f_hpf/fs))     # corresponding IIR coefficient 

    tau_in = 10e-3                # p329 transmitter creation time constant
    c_in = 1/(fs*tau_in)          # p329 corresponding IIR coefficient    
    tau_out = 0.5e-3              # p329 transmitter depletion time constant
    c_out = 1/(fs*tau_out)        # p329 corresponding IIR coefficient 
    tau_IHC = 80e-6               # p329 ~8kHz LPF for IHC output
    c_IHC = 1/(fs*tau_IHC)        # corresponding IIR coefficient 

    # OHC parameters
    scale = 0.1                   # p313 NLF parameter
    offset = 0.04                 # p313 NLF parameter
    b = 1.0                       # automatic gain loop feedback (1=no undamping).
    d_rz = 0.7*(1 - r1)           # p310 relative undamping

    # AGC loop parameters
    tau_AGC = .002 * 4**np.arange(4) # p336

    # The AGC filters are decimated, i.e., running at a lower sample rate
    c_AGC = 8 * 2**np.arange(4)/(fs*tau_AGC)

    # spatial filtering
    shift_AGC = c_AGC * 0.65 * np.sqrt(2)**np.arange(4)
    spread_sq_AGC = c_AGC * (1.65**2 + 1) * 2**np.arange(4)
    sa = (spread_sq_AGC + shift_AGC**2 - shift_AGC)/2
    sb = (spread_sq_AGC + shift_AGC**2 + shift_AGC)/2
    sc = 1 - sa - sb

    # initialise internal states
    W0 = np.zeros(num_sect)              # BM filter internal state
    W1 = np.zeros(num_sect)              # BM filter internal state
    W1old = np.zeros(num_sect)           # BM filter internal state at t-1
    BM = np.zeros((num_sect,npoints))    # BM displacement
    BM_hpf = np.zeros((num_sect,npoints))# BM displacement high-pass filtered at 20Hz
    trans = np.ones(num_sect)            # transmitter available
    IHC = np.zeros((num_sect,npoints))   # IHC output
    IHCa = np.zeros((num_sect,npoints))  # IHC filter internal state
    In8 = np.zeros(num_sect)             # Accumulator for ACG4
    In16 = np.zeros(num_sect)            # Accumulator for AGC3
    In32 = np.zeros(num_sect)            # Accumulator for AGC2
    In64 = np.zeros(num_sect)            # Accumulator for AGC1
    AGC = np.zeros((num_sect,npoints))   # AGC filter internal state
    AGC0 = np.zeros(num_sect)            # AGC filter internal state
    AGC1 = np.zeros(num_sect)            # AGC filter internal state
    AGC2 = np.zeros(num_sect)            # AGC filter internal state
    AGC3 = np.zeros(num_sect)            # AGC filter internal state


    # play through cochlea
    BM[-1] = stimulus             # put stimulus at BM[-1] to provide input to BM[0]
    BM[-1,-1] = 0                 # hack to make BM_hpf[num_sect-1,0] work
    for t in range(npoints):
        for s in range(num_sect):     # multiplex through the sections to calculate BM filters
            W0new = BM[s-1,t] + r[s]*(a0[s]*W0[s] - c0[s]*W1[s])
            W1[s] = r[s]*(a0[s]*W1[s] + c0[s]*W0[s])
            W0[s] = W0new
            BM[s,t] = g[s]*(BM[s-1,t] + h[s]*W1[s])
        # to speed up simulation, operate on all sections simultaneously for what follows    
        BM_hpf[:,t] = q*(BM_hpf[:,t-1] + BM[:,t] - BM[:,t-1])            # high-pass filter
        z = (BM_hpf[:,t]+0.175).clip(0)                                  # nonlinear function for IHC
        v_mem = z**3/(z**3+z**2+0.1)                                     # nonlinear function for IHC
        IHC_new = v_mem*trans                                            # IHC output
        trans += c_in*(1-trans) - c_out*IHC_new                          # update amount of neuro transmitter
        IHCa[:,t] = (1-c_IHC)*IHCa[:,t-1] + c_IHC*IHC_new                # Low-pass filter once
        IHC[:,t] = (1-c_IHC)*IHC[:,t-1] + c_IHC*IHCa[:,t]                # Low-pass filter twice
        v_OHC = W1 - W1old                                               # OHC potential
        W1old = W1.copy()
        sqr=(v_OHC*scale+offset)**2
        NLF= 1/(1 + (scale*v_OHC + offset)**2)                           # nonlinear function for OHC
        In8 += IHC[:,t]/8.0                                              # accumulate input
        if t%64 == 0:                                                    # subsample AGC1 by factor 64
            AGC3 = (1-c_AGC[3])*AGC3 + c_AGC[3]*In64                     # LPF in time domain
            AGC3 = sa[3]*np.roll(AGC3,1) + sc[3]*AGC3 + sb[3]*np.roll(AGC3,-1) # LPF in spatial domain
            In64 *= 0                                                    # reset input accumulator
        if t%32 == 0:                                                    # subsample AGC2 by factor 32
            AGC2 = (1-c_AGC[2])*AGC2 + c_AGC[2]*(In32 + 2*AGC3)
            AGC2 = sa[2]*np.roll(AGC2,1) + sc[2]*AGC2 + sb[2]*np.roll(AGC2,-1)
            In64 += In32
            In32 *= 0
        if t%16 == 0:                                                    # subsample ACG3 by factor 16
            AGC1 = (1-c_AGC[1])*AGC1 + c_AGC[1]*(In16 + 2*AGC2)
            AGC1 = sa[1]*np.roll(AGC1,1) + sc[1]*AGC1 + sb[1]*np.roll(AGC1,-1)
            In32 += In16
            In16 *= 0
        if t%8 == 0:
            AGC0 = (1-c_AGC[0])*AGC0 + c_AGC[0]*(In8 + 2*AGC1)
            AGC0 = sa[0]*np.roll(AGC0,1) + sc[0]*AGC0 + sb[0]*np.roll(AGC0,-1)
            AGC[:,t] = AGC0                                              # store AGC output for plotting
            b = AGC0
            r = r1 + d_rz*(1-b)*NLF                                          # feedback to BM
            g = (1-2*a0*r+r*r)/(1-(2*a0-h*c0)*r+r*r)                         # gain for BM
            In16 += In8
            In8 *= 0
        else:
            AGC[:,t] = AGC[:,t-1]
        if show_progress and t%10000 == 0:
            print("%.1f%%, " % (100*t/npoints), end="") #
            sys.stdout.flush()
    print("100.0%. Done!")
    return (IHC, f)

def damped_env(T, tau, num_samples, fs):
    t_period = np.arange(0,T,1/fs) 
    period = np.exp(-t_period/tau)
    n_period = len(period)
    env = [period[k%n_period] for k in range(num_samples)]
    return env

################################################################################
# Main script
################################################################################
if __name__=="__main__":
    # fs = 32000
    # dur = 2.0
    # t = np.arange(0, fs*dur)/fs
    # f0 = 100.0
    # f1 = 10000.0
    # stimulus = dsp.chirp(t, f0, t[-1], f1, method='logarithmic', phi=-90)


    dur = 1.0
    fs = 32000
    f0 = 100.0
    dt = 1/fs
    t = np.arange(0,dur,dt)
    sig = np.sin(2*np.pi*f0*t)
    period = damped_env(0.10, 0.050, len(sig), fs)
    stimulus = sig*period
    
    num_sections = 100
    IHC_out, _ = carfac_agc(stimulus, fs, num_sections)

    num_points = len(t)
    fig = plt.figure()
    t_max = num_points/fs
    num_points = int(t_max*fs)
    t = np.arange(num_points)/fs
    p = 0.01
    for k in range(num_sections):
        plt.fill_between(t, 0, IHC_out[k,:num_points]+p*(num_sections-k), facecolor='w',
                edgecolor='k', linewidth=0.6)


    plt.show()

